from PIL import Image

print_width = 300
max_height = 1500


def print_image(filepath):
    print("printing " + filepath)
    try:
        im = Image.open(filepath)
        print("Image format: %r, size: %r, mode: %r" %
              (im.format, im.size, im.mode))
        wpercent = (print_width/float(im.size[0]))
        hsize = int((float(im.size[1])*float(wpercent)))
        im = im.resize((print_width, hsize), resample=Image.BICUBIC)
        print("Resized: size: %r, mode: %r" % (im.size, im.mode))
        if im.size[1] > max_height:
            return "Image too tall"
        # print the image here!
        im.show()
    except OSError:
        return "Image file unreadable (try a jpg?)"
