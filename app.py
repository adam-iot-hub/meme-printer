from flask import Flask

UPLOAD_FOLDER = 'upload/'

app = Flask(__name__)
app.secret_key = "super secret password"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 20 * 1024 * 1024
