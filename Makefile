export FLASK_APP = main.py

# add debug annotations, turn off optimizations, and #define DEBUG
# use in bash with `DEBUG=1 make <foo>`
# use in fish with `env DEBUG=1 make <foo>`
ifdef DEBUG
export FLASK_DEBUG = 1
endif

server:
	python -m flask run

external:
	python -m flask run -h "0.0.0.0"

default:
	server

clean:
	rm photos/*