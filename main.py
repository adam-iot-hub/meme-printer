import os
import urllib.request
from flask import Flask, flash, request, redirect, render_template
from werkzeug.utils import secure_filename
from datetime import datetime

from app import app
import printer

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def upload_form():
    return render_template('upload.html')


@app.route('/file-upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']

        # check if file is exists
        if file.filename == '':
            flash('No file selected for uploading')
            return redirect(request.url)

        # check for allowed extensions
        if not allowed_file(file.filename):
            flash('Allowed file types are png, jpg, jpeg, gif')
            return redirect(request.url)

        # finally, upload the file
        if file:
            directory = os.path.join(
                app.config['UPLOAD_FOLDER'], "photos/")
            extension = file.filename.rsplit('.', 1)[1].lower()
            og_filename = datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S")
            filename = og_filename
            filepath = os.path.join(
                directory, filename + "." + extension)

            i = 0
            while os.path.isfile(filepath):
                i += 1
                filename = og_filename + '-' + str(i)
                filepath = os.path.join(
                    directory, filename + "." + extension)

            print("saving: " + filepath)
            file.save(filepath)
            error = printer.print_image(filepath)
            if error:
                flash(error)
                return redirect('/')
            flash('Your meme is printing!')
            return redirect('/')


@app.route('/text-upload', methods=['POST'])
def upload_text():
    if request.method == 'POST':
        textLogFilepath = os.path.join(
            app.config['UPLOAD_FOLDER'], "text-log.txt")
        textToPrint = request.form['big-text']
        print("Now printing: " + textToPrint)
        with open(textLogFilepath, "a") as textLogFile:
            datestring = datetime.strftime(
                datetime.now(), "===== %Y%m%d-%H%M%S =====\n")
            textLogFile.write(datestring)
            textLogFile.write(textToPrint)
            textLogFile.write("\n")
        flash('Your text is printing!')
    return redirect('/')


if __name__ == "__main__":
    app.run()
