# Meme Printer

Print images or text on a thermal printer through the web.

### Setup
Install requirements (python3):
```
pip install -r requirements.txt
```
(I recommend using [virtualenv](http://sourabhbajaj.com/mac-setup/Python/virtualenv.html)).

### Running
Start the server with `make`. (See Makefile for other options).

### Todo:
- [ ] Implement thermal printer
- [ ] RTF for text input
- [ ] Add logging for photos
